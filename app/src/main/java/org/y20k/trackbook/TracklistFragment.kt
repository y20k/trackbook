/*
 * TracklistFragment.kt
 * Implements the TracklistFragment fragment
 * A TracklistFragment displays a list recorded tracks
 *
 * This file is part of
 * TRACKBOOK - Movement Recorder for Android
 *
 * Copyright (c) 2016-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 *
 * Trackbook uses osmdroid - OpenStreetMap-Tools for Android
 * https://github.com/osmdroid/osmdroid
 */


package org.y20k.trackbook

import YesNoDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.net.toUri
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.launch
import org.y20k.trackbook.core.TracklistElement
import org.y20k.trackbook.helpers.UiHelper
import org.y20k.trackbook.tracklist.TracklistAdapter
import org.y20k.transistor.helpers.BackupHelper


/*
 * TracklistFragment class
 */
class TracklistFragment : Fragment(), TracklistAdapter.TracklistAdapterListener, YesNoDialog.YesNoDialogListener {

    /* Define log tag */
    private val TAG: String = TracklistFragment::class.java.simpleName


    /* Main class variables */
    private lateinit var tracklistAdapter: TracklistAdapter
    private lateinit var trackElementList: RecyclerView
    private lateinit var tracklistOnboarding: ConstraintLayout


    /* Overrides onCreateView from Fragment */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // create tracklist adapter
        tracklistAdapter = TracklistAdapter(this)
    }


    /* Overrides onCreateView from Fragment */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // find views
        val rootView = inflater.inflate(R.layout.fragment_tracklist, container, false)
        trackElementList = rootView.findViewById(R.id.track_element_list)
        tracklistOnboarding = rootView.findViewById(R.id.track_list_onboarding)

        // set up recycler view
        trackElementList.layoutManager = CustomLinearLayoutManager(activity as Context)
        trackElementList.itemAnimator = DefaultItemAnimator()
        trackElementList.adapter = tracklistAdapter

        // enable swipe to delete
        val swipeHandler = object : UiHelper.SwipeToDeleteCallback(activity as Context) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                // ask user
                val adapterPosition: Int = viewHolder.adapterPosition // first position in list is reserved for statistics
                val dialogMessage: String = "${getString(R.string.dialog_yes_no_message_delete_recording)}\n\n- ${tracklistAdapter.getTrackName(adapterPosition)}"
                YesNoDialog(this@TracklistFragment as YesNoDialog.YesNoDialogListener).show(context = activity as Context, type = Keys.DIALOG_DELETE_TRACK, messageString = dialogMessage, yesButton = R.string.dialog_yes_no_positive_button_delete_recording, payload = adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(trackElementList)

        // toggle onboarding layout
        toggleOnboardingLayout()

        return rootView
    }


    /* Overrides onResume from Fragment */
    override fun onResume() {
        super.onResume()
        handleNavigationArguments()
    }


    /* Overrides onTrackElementTapped from TracklistElementAdapterListener */
    override fun onTrackElementTapped(tracklistElement: TracklistElement) {
        val bundle: Bundle = bundleOf(
            Keys.ARG_TRACK_TITLE to tracklistElement.name,
            Keys.ARG_TRACK_FILE_URI to tracklistElement.trackUriString,
            Keys.ARG_GPX_FILE_URI to tracklistElement.gpxUriString,
            Keys.ARG_TRACK_ID to tracklistElement.getTrackId()
        )
        val options: NavOptions = NavOptions.Builder().setPopUpTo(R.id.tracklist_fragment, true).build()
        findNavController().navigate(R.id.action_tracklist_fragment_to_track_fragment, bundle, options)
    }


    /* Overrides onYesNoDialog from YesNoDialogListener */
    override fun onYesNoDialog(type: Int, dialogResult: Boolean, payload: Int, payloadString: String) {
        when (type) {
            Keys.DIALOG_DELETE_TRACK -> {
                when (dialogResult) {
                    // user tapped remove track
                    true -> {
                        lifecycleScope.launch {
                            tracklistAdapter.removeTrackAtPosition(this, payload)
                        }
                        toggleOnboardingLayout()
                    }
                    // user tapped cancel
                    false -> {
                        tracklistAdapter.notifyItemChanged(payload)
                    }
                }
            }
            Keys.DIALOG_RESTORE_RECORDINGS -> {
                when (dialogResult) {
                    // user tapped restore
                    true -> {
                        BackupHelper.restore(activity as Context, payloadString.toUri())
                        tracklistAdapter.reload()
                    }
                    // user tapped cancel
                    false -> { /* do nothing */ }
                }
            }
            Keys.DIALOG_DELETE_NON_STARRED -> {
                if (dialogResult) {
                    // user tapped delete non starred
                    lifecycleScope.launch {
                        tracklistAdapter.removeNonStarred(this)
                    }
                }
            }
            else -> {
                super.onYesNoDialog(type, dialogResult, payload, payloadString)
            }
        }
    }


    // toggle onboarding layout
    private fun toggleOnboardingLayout() {
        when (tracklistAdapter.isEmpty()) {
            true -> {
                // show onboarding layout
                tracklistOnboarding.visibility = View.VISIBLE
                trackElementList.visibility = View.GONE
            }
            false -> {
                // hide onboarding layout
                tracklistOnboarding.visibility = View.GONE
                trackElementList.visibility = View.VISIBLE
            }
        }
    }


    /* Handles arguments handed over by navigation (from SettingsFragment) */
    private fun handleNavigationArguments() {
        // CASE: delete non-starred recordings
        if (arguments?.containsKey(Keys.ARG_DELETE_NON_STARRED) == true) {
            arguments?.remove(Keys.ARG_DELETE_NON_STARRED)
            YesNoDialog(this as YesNoDialog.YesNoDialogListener).show(context = activity as Context, type = Keys.DIALOG_DELETE_NON_STARRED, message = R.string.dialog_yes_no_message_delete_non_starred, yesButton = R.string.dialog_yes_no_positive_button_delete_non_starred)
        }
        // CASE: restore recorded tracks
        else if (arguments?.containsKey(Keys.ARG_RESTORE_RECORDINGS) == true) {
            val restoreCollectionFileString: String? = arguments?.getString(Keys.ARG_RESTORE_RECORDINGS)
            arguments?.remove(Keys.ARG_RESTORE_RECORDINGS)
            if (!restoreCollectionFileString.isNullOrEmpty()) {
                when (tracklistAdapter.isEmpty()) {
                    true -> {
                        BackupHelper.restore(activity as Context, restoreCollectionFileString.toUri())
                        tracklistAdapter.reload()
                    }
                    false -> {
                        YesNoDialog(this as YesNoDialog.YesNoDialogListener).show(
                            context = activity as Context,
                            type = Keys.DIALOG_RESTORE_RECORDINGS,
                            messageString = getString(R.string.dialog_restore_replace_existing),
                            payloadString = restoreCollectionFileString
                        )
                    }
                }
            }
        }
    }


    /*
     * Inner class: custom LinearLayoutManager that overrides onLayoutCompleted
     */
    inner class CustomLinearLayoutManager(context: Context): LinearLayoutManager(context, VERTICAL, false) {

        override fun supportsPredictiveItemAnimations(): Boolean {
            return true
        }

        override fun onLayoutCompleted(state: RecyclerView.State?) {
            super.onLayoutCompleted(state)
            // handle delete request from TrackFragment - after layout calculations are complete
            val deleteTrackId: Long = arguments?.getLong(Keys.ARG_TRACK_ID, -1L) ?: -1L
            arguments?.putLong(Keys.ARG_TRACK_ID, -1L)
            if (deleteTrackId != -1L) {
                lifecycleScope.launch {
                    tracklistAdapter.removeTrackById(this, deleteTrackId)
                    toggleOnboardingLayout()
                }
            }
        }

    }
    /*
     * End of inner class
     */

}
