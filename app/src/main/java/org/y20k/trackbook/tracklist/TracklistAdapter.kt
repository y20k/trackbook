/*
 * TracklistAdapter.kt
 * Implements the TracklistAdapter class
 * A TracklistAdapter is a custom adapter for a RecyclerView
 *
 * This file is part of
 * TRACKBOOK - Movement Recorder for Android
 *
 * Copyright (c) 2016-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 *
 * Trackbook uses osmdroid - OpenStreetMap-Tools for Android
 * https://github.com/osmdroid/osmdroid
 */


package org.y20k.trackbook.tracklist


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.y20k.trackbook.Keys
import org.y20k.trackbook.R
import org.y20k.trackbook.core.Tracklist
import org.y20k.trackbook.core.TracklistElement
import org.y20k.trackbook.helpers.FileHelper
import org.y20k.trackbook.helpers.LengthUnitHelper
import org.y20k.trackbook.helpers.PreferencesHelper
import org.y20k.trackbook.helpers.TrackHelper
import java.util.GregorianCalendar


/*
 * TracklistAdapter class
 */
class TracklistAdapter(private val fragment: Fragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /* Define log tag */
    private val TAG: String = TracklistAdapter::class.java.simpleName


    /* Main class variables */
    private val context: Context = fragment.activity as Context
    private lateinit var tracklistListener: TracklistAdapterListener
    private var useImperial: Boolean = PreferencesHelper.loadUseImperialUnits()
    var tracklist: Tracklist = Tracklist()


    /* Listener Interface */
    interface TracklistAdapterListener {
        fun onTrackElementTapped(tracklistElement: TracklistElement) {  }
        // fun onTrackElementStarred(trackId: Long, starred: Boolean)
    }


    /* Overrides onAttachedToRecyclerView from RecyclerView.Adapter */
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        // get reference to listener
        tracklistListener = fragment as TracklistAdapterListener
        // load tracklist
        loadTracklist()
    }


    /* Overrides onCreateViewHolder from RecyclerView.Adapter */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        when (viewType) {
            Keys.VIEW_TYPE_STATISTICS -> {
                val v = LayoutInflater.from(parent.context).inflate(R.layout.element_statistics, parent, false)
                return ElementStatisticsViewHolder(v)
            }
            else -> {
                val v = LayoutInflater.from(parent.context).inflate(R.layout.element_track, parent, false)
                return ElementTrackViewHolder(v)
            }
        }
    }


    /* Overrides getItemViewType */
    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return Keys.VIEW_TYPE_STATISTICS
        } else {
            return Keys.VIEW_TYPE_TRACK
        }
    }


    /* Overrides getItemCount from RecyclerView.Adapter */
    override fun getItemCount(): Int {
        // +1 ==> the total statistics element
        return tracklist.tracklistElements.size + 1
    }


    /* Overrides onBindViewHolder from RecyclerView.Adapter */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {

            // CASE STATISTICS ELEMENT
            is ElementStatisticsViewHolder -> {
                val elementStatisticsViewHolder: ElementStatisticsViewHolder = holder as ElementStatisticsViewHolder
                elementStatisticsViewHolder.totalDistanceView.text = LengthUnitHelper.convertDistanceToString(tracklist.totalDistanceAll, useImperial)
            }

            // CASE TRACK ELEMENT
            is ElementTrackViewHolder -> {
                val positionInTracklist: Int = holder.layoutPosition -1
                val elementTrackViewHolder: ElementTrackViewHolder = holder as ElementTrackViewHolder
                elementTrackViewHolder.trackNameView.text = tracklist.tracklistElements[positionInTracklist].name
                elementTrackViewHolder.trackDataView.text = createTrackDataString(positionInTracklist)
                when (tracklist.tracklistElements[positionInTracklist].starred) {
                    true -> elementTrackViewHolder.starButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_filled_24dp))
                    false -> elementTrackViewHolder.starButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_outline_24dp))
                }
                elementTrackViewHolder.trackElement.setOnClickListener {
                    tracklistListener.onTrackElementTapped(tracklist.tracklistElements[positionInTracklist])
                }
                elementTrackViewHolder.starButton.setOnClickListener {
                    toggleStarred(it, positionInTracklist)
                }
            }

        }

    }


    /* Get track name for given position */
    fun getTrackName(positionInRecyclerView: Int): String {
        // first position is always the statistics element
        return tracklist.tracklistElements[positionInRecyclerView - 1].name
    }


    /* Removes track and track files for given position - used by TracklistFragment */
    fun removeTrackAtPosition(scope: CoroutineScope, position: Int) {
        scope.launch {
            val positionInTracklist = position - 1
            tracklist = FileHelper.deleteTrack(context, positionInTracklist, tracklist)
            withContext(Main) {
                notifyItemRemoved(position)
                notifyItemChanged(0)
            }
        }
    }


    /* Removes track and track files for given track id - used by TracklistFragment */
    fun removeTrackById(scope: CoroutineScope, trackId: Long) {
        scope.launch {
            val positionInTracklist: Int = findPosition(trackId)
            tracklist = FileHelper.deleteTrack(context, positionInTracklist, tracklist)
            // wait for result and store in tracklist
            withContext(Main) {
                val positionInRecyclerView: Int = positionInTracklist + 1 // position 0 is the statistics element
                notifyItemRemoved(positionInRecyclerView)
                notifyItemChanged(0)
            }
        }
    }


    /* Removes non starred track recordings from tracklist and deletes them */
    fun removeNonStarred(scope: CoroutineScope) {
        scope.launch {
            val oldTracklist: Tracklist = tracklist.deepCopy()
            val newTracklist: Tracklist = FileHelper.deleteNonStarred(context, tracklist)
            updateRecyclerView(oldTracklist, newTracklist)
        }
    }


    /* Updates the tracklist - redraws the views with changed content */
    private fun updateRecyclerView(oldTracklist: Tracklist, newTracklist: Tracklist) {
        tracklist = newTracklist
        // calculate differences between current collection and new collection - and inform this adapter about the changes
        val diffResult = DiffUtil.calculateDiff(DiffCallback(oldTracklist, newTracklist), true)
        diffResult.dispatchUpdatesTo(this@TracklistAdapter)
    }


    /* Returns if the adapter is empty */
    fun isEmpty(): Boolean {
        return tracklist.tracklistElements.size == 0
    }


    /* Reload all recordings from storage - replaces the current tracklist */
    fun reload() {
        fragment.lifecycleScope.launch {
            val oldTracklist: Tracklist = tracklist.deepCopy()
            loadTracklist()
            val newTracklist: Tracklist = tracklist
            withContext(Main) {
                updateRecyclerView(oldTracklist, newTracklist)
            }
        }
    }


    /* Finds current position of track element in adapter list */
    private fun findPosition(trackId: Long): Int {
        tracklist.tracklistElements.forEachIndexed {index, tracklistElement ->
            if (tracklistElement.getTrackId() == trackId) return index
        }
        return -1
    }


    /* Toggles the starred state of tracklist element - and saves tracklist */
    private fun toggleStarred(view: View, position: Int) {
        val starButton: ImageButton = view as ImageButton
        when (tracklist.tracklistElements[position].starred) {
            true -> {
                starButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_outline_24dp))
                tracklist.tracklistElements[position].starred = false
            }
            false -> {
                starButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_filled_24dp))
                tracklist.tracklistElements[position].starred = true
            }
        }
        fragment.lifecycleScope.launch {
            withContext(IO) {
                FileHelper.saveTracklist(context, tracklist, GregorianCalendar.getInstance().time)
            }
        }
    }


    /* Creates the track data string */
    private fun createTrackDataString(position: Int): String {
        val tracklistElement: TracklistElement = tracklist.tracklistElements[position]
        val trackDataString: String
        when (tracklistElement.name == tracklistElement.dateString) {
            // CASE: no individual name set - exclude date
            true -> trackDataString = "${LengthUnitHelper.convertDistanceToString(tracklistElement.length, useImperial)} • ${tracklistElement.durationString}"
            // CASE: no individual name set - include date
            false -> trackDataString = "${tracklistElement.dateString} • ${LengthUnitHelper.convertDistanceToString(tracklistElement.length, useImperial)} • ${tracklistElement.durationString}"
        }
        return trackDataString
    }


    /* Load recordings from storage */
    private fun loadTracklist() {
        tracklist = FileHelper.readTracklist(context)
        tracklist.tracklistElements.sortByDescending { tracklistElement -> tracklistElement.date  }
        // calculate total duration and distance, if necessary
        if (tracklist.tracklistElements.isNotEmpty() && tracklist.totalDurationAll == 0L) {
            TrackHelper.calculateAndSaveTrackTotals(context, tracklist)
        }
    }


    /*
     * Inner class: DiffUtil.Callback that determines changes in data - improves list performance
     * Note: position 0 is reserved for the statistics card
     */
    private inner class DiffCallback(val oldList: Tracklist, val newList: Tracklist): DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            if (oldItemPosition == 0 || newItemPosition == 0) return false
            val oldItem: TracklistElement = oldList.tracklistElements[oldItemPosition - 1]
            val newItem: TracklistElement = newList.tracklistElements[newItemPosition - 1]
            return oldItem.getTrackId() == newItem.getTrackId()
        }

        override fun getOldListSize(): Int {
            return oldList.tracklistElements.size + 1
        }

        override fun getNewListSize(): Int {
            return newList.tracklistElements.size + 1
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            if (oldItemPosition == 0 || newItemPosition == 0) return false

            val oldItem = oldList.tracklistElements[oldItemPosition - 1]
            val newItem = newList.tracklistElements[newItemPosition - 1]

            if (oldItem.getTrackId() != newItem.getTrackId()) return false
            if (oldItem.getTrackId() != newItem.getTrackId() && (oldItem.starred != newItem.starred)) return false
            if (oldItem.getTrackId() != newItem.getTrackId() && (oldItem.length != newItem.length)) return false

            return true
        }
    }
    /*
     * End of inner class
     */


    /*
     * Inner class: ViewHolder for a track element
     */
    inner class ElementTrackViewHolder (elementTrackLayout: View): RecyclerView.ViewHolder(elementTrackLayout) {
        val trackElement: ConstraintLayout = elementTrackLayout.findViewById(R.id.track_element)
        val trackNameView: TextView = elementTrackLayout.findViewById(R.id.track_name)
        val trackDataView: TextView = elementTrackLayout.findViewById(R.id.track_data)
        val starButton: ImageButton = elementTrackLayout.findViewById(R.id.star_button)
    }
    /*
     * End of inner class
     */


    /*
     * Inner class: ViewHolder for a statistics element
     */
    inner class ElementStatisticsViewHolder (elementStatisticsLayout: View): RecyclerView.ViewHolder(elementStatisticsLayout) {
        val totalDistanceView: TextView = elementStatisticsLayout.findViewById(R.id.total_distance_data)
    }
    /*
     * End of inner class
     */

}
