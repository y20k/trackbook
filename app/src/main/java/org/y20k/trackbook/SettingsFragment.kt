/*
 * SettingsFragment.kt
 * Implements the SettingsFragment fragment
 * A SettingsFragment displays the user accessible settings of the app
 *
 * This file is part of
 * TRACKBOOK - Movement Recorder for Android
 *
 * Copyright (c) 2016-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 *
 * Trackbook uses osmdroid - OpenStreetMap-Tools for Android
 * https://github.com/osmdroid/osmdroid
 */


package org.y20k.trackbook


import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.documentfile.provider.DocumentFile
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import androidx.preference.contains
import com.google.android.material.snackbar.Snackbar
import org.y20k.trackbook.helpers.AppThemeHelper
import org.y20k.trackbook.helpers.FileHelper
import org.y20k.trackbook.helpers.LengthUnitHelper
import org.y20k.trackbook.helpers.PreferencesHelper
import org.y20k.transistor.helpers.BackupHelper


/*
 * SettingsFragment class
 */
class SettingsFragment : PreferenceFragmentCompat() {

    /* Define log tag */
    private val TAG: String = SettingsFragment::class.java.simpleName


    /* Main class variables */
    lateinit var preferenceGpsOnly: SwitchPreferenceCompat
    lateinit var preferenceImperialMeasurementUnits: SwitchPreferenceCompat
    lateinit var preferenceThemeSelection: ListPreference
    lateinit var preferenceBackupRecordings: Preference
    lateinit var preferenceRestoreRecordings: Preference
    lateinit var preferenceDeleteNonStarred: Preference
    lateinit var preferenceRecordingAccuracy: SwitchPreferenceCompat
    lateinit var preferenceMapProvider: SwitchPreferenceCompat
    lateinit var preferenceOnDeviceMapsFolder: Preference
    lateinit var preferenceShowAppInfo: Preference
    lateinit var preferenceAppVersion: Preference
    lateinit var onDeviceMapFiles: List<DocumentFile>
    lateinit var onDeviceMapsFolderName: String


    /* Overrides onViewCreated from PreferenceFragmentCompat */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // set the background color
        view.setBackgroundColor(resources.getColor(R.color.app_window_background, null))
    }


    /* Overrides onCreatePreferences from PreferenceFragmentCompat */
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {

        val screen = preferenceManager.createPreferenceScreen(preferenceManager.context)

        onDeviceMapFiles = FileHelper.getOnDeviceMapFiles(activity as Context)
        onDeviceMapsFolderName = FileHelper.getOnDeviceMapsFolderName(activity as Context)

        // set up "Restrict to GPS" preference
        preferenceGpsOnly = SwitchPreferenceCompat(activity as Context)
        preferenceGpsOnly.title = getString(R.string.pref_gps_only_title)
        preferenceGpsOnly.setIcon(R.drawable.ic_gps_24dp)
        preferenceGpsOnly.key = Keys.PREF_GPS_ONLY
        preferenceGpsOnly.summaryOn = getString(R.string.pref_gps_only_summary_gps_only)
        preferenceGpsOnly.summaryOff = getString(R.string.pref_gps_only_summary_gps_and_network)
        preferenceGpsOnly.setDefaultValue(false)

        // set up "Use Imperial Measurements" preference
        preferenceImperialMeasurementUnits = SwitchPreferenceCompat(activity as Context)
        preferenceImperialMeasurementUnits.title = getString(R.string.pref_imperial_measurement_units_title)
        preferenceImperialMeasurementUnits.setIcon(R.drawable.ic_square_foot_24px)
        preferenceImperialMeasurementUnits.key = Keys.PREF_USE_IMPERIAL_UNITS
        preferenceImperialMeasurementUnits.summaryOn = getString(R.string.pref_imperial_measurement_units_summary_imperial)
        preferenceImperialMeasurementUnits.summaryOff = getString(R.string.pref_imperial_measurement_units_summary_metric)
        preferenceImperialMeasurementUnits.setDefaultValue(LengthUnitHelper.useImperialUnits())

        // set up "App Theme" preference
        preferenceThemeSelection = ListPreference(activity as Context)
        preferenceThemeSelection.title = getString(R.string.pref_theme_selection_title)
        preferenceThemeSelection.setIcon(R.drawable.ic_smartphone_24dp)
        preferenceThemeSelection.key = Keys.PREF_THEME_SELECTION
        preferenceThemeSelection.summary = "${getString(R.string.pref_theme_selection_summary)} ${AppThemeHelper.getCurrentTheme(activity as Context)}"
        preferenceThemeSelection.entries = arrayOf(getString(R.string.pref_theme_selection_mode_device_default), getString(R.string.pref_theme_selection_mode_light), getString(R.string.pref_theme_selection_mode_dark))
        preferenceThemeSelection.entryValues = arrayOf(Keys.STATE_THEME_FOLLOW_SYSTEM, Keys.STATE_THEME_LIGHT_MODE, Keys.STATE_THEME_DARK_MODE)
        preferenceThemeSelection.setOnPreferenceChangeListener { preference, newValue ->
            if (preference is ListPreference) {
                val index: Int = preference.entryValues.indexOf(newValue)
                preferenceThemeSelection.summary = "${getString(R.string.pref_theme_selection_summary)} ${preference.entries[index]}"
                return@setOnPreferenceChangeListener true
            } else {
                return@setOnPreferenceChangeListener false
            }
        }

        // set up "Backup Recordings" preference
        preferenceBackupRecordings = Preference(activity as Context)
        preferenceBackupRecordings.title = getString(R.string.pref_backup_title)
        preferenceBackupRecordings.setIcon(R.drawable.ic_save_to_storage_24dp)
        preferenceBackupRecordings.summary = getString(R.string.pref_backup_summary)
        preferenceBackupRecordings.setOnPreferenceClickListener {
            openBackupRecordingsDialog()
            return@setOnPreferenceClickListener true
        }

        // set up "Restore Recordings" preference
        preferenceRestoreRecordings = Preference(activity as Context)
        preferenceRestoreRecordings.title = getString(R.string.pref_restore_title)
        preferenceRestoreRecordings.setIcon(R.drawable.ic_restore_24dp)
        preferenceRestoreRecordings.summary = getString(R.string.pref_restore_summary)
        preferenceRestoreRecordings.setOnPreferenceClickListener {
            openRestoreRecordingsDialog()
            return@setOnPreferenceClickListener true
        }

        // set up "Delete Non-Starred" preference
        preferenceDeleteNonStarred = Preference(activity as Context)
        preferenceDeleteNonStarred.title = getString(R.string.pref_delete_non_starred_title)
        preferenceDeleteNonStarred.setIcon(R.drawable.ic_delete_24dp)
        preferenceDeleteNonStarred.summary = getString(R.string.pref_delete_non_starred_summary)
        preferenceDeleteNonStarred.setOnPreferenceClickListener{
            openDeleteNonStarredDialog()
            return@setOnPreferenceClickListener true
        }

        // set up "Recording Accuracy" preference
        preferenceRecordingAccuracy = SwitchPreferenceCompat(activity as Context)
        preferenceRecordingAccuracy.title = getString(R.string.pref_recording_accuracy_title)
        preferenceRecordingAccuracy.setIcon(R.drawable.ic_straighten_24dp)
        preferenceRecordingAccuracy.key = Keys.PREF_RECORDING_ACCURACY_HIGH
        preferenceRecordingAccuracy.summaryOn = getString(R.string.pref_recording_accuracy_summary_high)
        preferenceRecordingAccuracy.summaryOff = getString(R.string.pref_recording_accuracy_summary_default)
        preferenceRecordingAccuracy.setDefaultValue(false)

        // set up "Map Provider" preference
        preferenceMapProvider = SwitchPreferenceCompat(activity as Context)
        preferenceMapProvider.title = "Map Provider"
        preferenceMapProvider.setIcon(R.drawable.ic_map_24dp)
        preferenceMapProvider.key = Keys.PREF_ON_DEVICE_MAPS
        preferenceMapProvider.summaryOn = "Currently using: On-device maps (configure source folder below)"
        preferenceMapProvider.summaryOff = "Currently using: Online maps (default)"
        preferenceMapProvider.setDefaultValue(false)
        preferenceMapProvider.setOnPreferenceClickListener {
            // open the folder chooser, if on-device maps is selected and folder does not contain any .map files
            if (preferenceMapProvider.isChecked && onDeviceMapFiles.isEmpty()) {
                openOnDeviceMapsFolderDialog()
            } else {
                // toggle the visibility of preferenceOnDeviceMapsFolder
                preferenceOnDeviceMapsFolder.isVisible = preferenceMapProvider.isChecked
            }
            return@setOnPreferenceClickListener true
        }

        // set up "On-device Maps" preference
        preferenceOnDeviceMapsFolder = Preference(activity as Context)
        preferenceOnDeviceMapsFolder.title = "On-device Maps Folder"
        preferenceOnDeviceMapsFolder.setIcon(R.drawable.ic_folder_24dp)
        preferenceOnDeviceMapsFolder.summary = "Currently using the .map files from this folder:\n/${onDeviceMapsFolderName}/ (tap to change folder)"
        preferenceOnDeviceMapsFolder.setOnPreferenceClickListener{
            openOnDeviceMapsFolderDialog()
            return@setOnPreferenceClickListener true
        }

        // set up "Permissions" preference
        preferenceShowAppInfo = Preference(activity as Context)
        preferenceShowAppInfo.title = getString(R.string.pref_show_app_info_title)
        preferenceShowAppInfo.setIcon(R.drawable.ic_open_in_new_24dp)
        preferenceShowAppInfo.summary = getString(R.string.pref_show_app_info_summary)
        preferenceShowAppInfo.setOnPreferenceClickListener{
            // open "App info" screen in global settings
            val intent = Intent().apply {
                action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                data = Uri.fromParts("package", requireContext().packageName, null)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            }
            startActivity(intent)
            return@setOnPreferenceClickListener true
        }

        // set up "App Version" preference
        preferenceAppVersion = Preference(activity as Context)
        preferenceAppVersion.title = getString(R.string.pref_app_version_title)
        preferenceAppVersion.setIcon(R.drawable.ic_info_24dp)
        preferenceAppVersion.summary = "${getString(R.string.pref_app_version_summary)} ${BuildConfig.VERSION_NAME} (${getString(R.string.app_version_name)})"
        preferenceAppVersion.setOnPreferenceClickListener {
            // copy to clipboard
            val clip: ClipData = ClipData.newPlainText("simple text", preferenceAppVersion.summary)
            val cm: ClipboardManager = (activity as Context).getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cm.setPrimaryClip(clip)
            Toast.makeText(activity as Context, R.string.toast_message_copied_to_clipboard, Toast.LENGTH_LONG).show()
            return@setOnPreferenceClickListener true
        }


        // set preference categories
        val preferenceCategoryGeneral: PreferenceCategory = PreferenceCategory(activity as Context)
        preferenceCategoryGeneral.title = getString(R.string.pref_general_title)
        preferenceCategoryGeneral.contains(preferenceImperialMeasurementUnits)
        preferenceCategoryGeneral.contains(preferenceGpsOnly)
        val preferenceCategoryMaintenance: PreferenceCategory = PreferenceCategory(activity as Context)
        preferenceCategoryMaintenance.title = getString(R.string.pref_maintenance_title)
        preferenceCategoryMaintenance.contains(preferenceBackupRecordings)
        preferenceCategoryMaintenance.contains(preferenceRestoreRecordings)
        preferenceCategoryMaintenance.contains(preferenceDeleteNonStarred)

        val preferenceCategoryAdvanced: PreferenceCategory = PreferenceCategory(activity as Context)
        preferenceCategoryAdvanced.title = getString(R.string.pref_advanced_title)
        preferenceCategoryAdvanced.contains(preferenceRecordingAccuracy)
        preferenceCategoryAdvanced.contains(preferenceMapProvider)
        preferenceCategoryAdvanced.contains(preferenceOnDeviceMapsFolder)
        preferenceCategoryAdvanced.contains(preferenceShowAppInfo)

        val preferenceCategoryAbout: PreferenceCategory = PreferenceCategory(activity as Context)
        preferenceCategoryAbout.title = getString(R.string.pref_about_title)
        preferenceCategoryAbout.contains(preferenceAppVersion)

        // setup preference screen
        screen.addPreference(preferenceCategoryGeneral)
        screen.addPreference(preferenceGpsOnly)
        screen.addPreference(preferenceImperialMeasurementUnits)
        screen.addPreference(preferenceThemeSelection)
        screen.addPreference(preferenceCategoryMaintenance)
        screen.addPreference(preferenceBackupRecordings)
        screen.addPreference(preferenceRestoreRecordings)
        screen.addPreference(preferenceDeleteNonStarred)
        screen.addPreference(preferenceCategoryAdvanced)
        screen.addPreference(preferenceRecordingAccuracy)
        screen.addPreference(preferenceMapProvider)
        screen.addPreference(preferenceOnDeviceMapsFolder)
        screen.addPreference(preferenceShowAppInfo)
        screen.addPreference(preferenceCategoryAbout)
        screen.addPreference(preferenceAppVersion)

        updateOnDeviceMapsPreferencesState()

        preferenceScreen = screen
    }


    /* Toggle the visibility of the "On-device Maps Folder" preference and reset the "Map Provider" switch */
    private fun updateOnDeviceMapsPreferencesState() {
        // get the current on device map files and the name of their folder
        onDeviceMapFiles = FileHelper.getOnDeviceMapFiles(preferenceManager.context)
        onDeviceMapsFolderName = FileHelper.getOnDeviceMapsFolderName(preferenceManager.context)
        // show/hide the "On-device Maps Folder" preference
        preferenceOnDeviceMapsFolder.isVisible = preferenceMapProvider.isChecked && onDeviceMapFiles.isNotEmpty()
        // toggle the "Map Provider" switch
        preferenceMapProvider.isChecked = preferenceOnDeviceMapsFolder.isVisible
        // update the summary
        if (preferenceMapProvider.isChecked) {
            preferenceOnDeviceMapsFolder.summary = "Currently using the .map files from this folder:\n/${onDeviceMapsFolderName}/ (tap to change folder)"
        }
    }


    /* Navigates to tracklist - show dialog confirmation dialog there */
    private fun openDeleteNonStarredDialog() {
        val bundle: Bundle = bundleOf(Keys.ARG_DELETE_NON_STARRED to true)
        val options: NavOptions = NavOptions.Builder().setPopUpTo(R.id.settings_fragment, true).build()
        this@SettingsFragment.findNavController().navigate(R.id.action_settings_fragment_to_tracklist_fragment, bundle, options)
    }


    /* Register the ActivityResultLauncher for the backup dialog */
    private val requestBackupRecordingsLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), this::requestBackupRecordingsResult)


    /* Opens up a file picker to select the backup location */
    private fun openBackupRecordingsDialog() {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = Keys.MIME_TYPE_ZIP
            putExtra(Intent.EXTRA_TITLE, Keys.RECORDINGS_BACKUP_FILE)
        }
        // file gets saved in the ActivityResult
        try {
            requestBackupRecordingsLauncher.launch(intent)
        } catch (exception: Exception) {
            Log.e(TAG, "Unable to select a backup location.\n$exception")
            Toast.makeText(activity as Context, R.string.toast_message_install_file_helper, Toast.LENGTH_LONG).show()
        }
    }


    /* Get the activity result from the backup recordings dialog */
    private fun requestBackupRecordingsResult(result: ActivityResult) {
        // save recordings backup file to result file location
        if (result.resultCode == Activity.RESULT_OK && result.data != null) {
            val targetUri: Uri? = result.data?.data
            if (targetUri != null) {
                BackupHelper.backup(activity as Context, targetUri)
            } else {
                Log.w(TAG, "Recordings backup failed.")
            }
        }
    }


    /* Register the ActivityResultLauncher for the restore dialog */
    private val requestRestoreRecordingsLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), this::requestRestoreRecordingsResult)


    /* Opens up a file picker to select the file containing the recorded tracks to be restored */
    private fun openRestoreRecordingsDialog() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "*/*"
            putExtra(Intent.EXTRA_MIME_TYPES, Keys.MIME_TYPES_ZIP)
        }
        // file gets saved in the ActivityResult
        try {
            requestRestoreRecordingsLauncher.launch(intent)
        } catch (exception: Exception) {
            Log.e(TAG, "Unable to open file picker for ZIP.\n$exception")
            // Toast.makeText(activity as Context, R.string.toast_message_install_file_helper, Toast.LENGTH_LONG).show()
        }
    }


    /* Get the activity result from the restore recordings dialog */
    private fun requestRestoreRecordingsResult(result: ActivityResult) {
        // save recordings backup file to result file location
        if (result.resultCode == Activity.RESULT_OK && result.data != null) {
            val sourceUri: Uri? = result.data?.data
            if (sourceUri != null) {
                // open and import recordings into tracklist
                val bundle: Bundle = bundleOf(Keys.ARG_RESTORE_RECORDINGS to "$sourceUri")
                val options: NavOptions = NavOptions.Builder().setPopUpTo(R.id.settings_fragment, true).build()
                this@SettingsFragment.findNavController().navigate(R.id.action_settings_fragment_to_tracklist_fragment, bundle, options)
            }
        }
    }


    /* Register the ActivityResultLauncher for the Select On Device Folder dialog */
    private val requestOnDeviceMapsFolderLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), this::requestOnDeviceMapsFolderResult)


    /* Opens up a file picker to select the folder containing the on-device map files */
    private fun openOnDeviceMapsFolderDialog() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
        }
        // folder gets saved in the ActivityResult
        try {
            requestOnDeviceMapsFolderLauncher.launch(intent)
        } catch (exception: Exception) {
            Log.e(TAG, "Unable to select a on-device maps folder.\n$exception")
            Toast.makeText(activity as Context, R.string.toast_message_install_file_helper, Toast.LENGTH_LONG).show()
        }
    }


    /* Get the activity result from the on-device folder dialog */
    private fun requestOnDeviceMapsFolderResult(result: ActivityResult) {
        // save location of the on-device maps folder
        if (result.resultCode == Activity.RESULT_OK && result.data != null) {
            val uri: Uri? = result.data?.data
            if (uri != null) {
                FileHelper.makeUriPersistent(preferenceManager.context, uri)
                PreferencesHelper.saveOnDeviceMapsFolder(uri.toString())
                updateOnDeviceMapsPreferencesState()
                if (onDeviceMapFiles.isEmpty()) {
                    showEmptyFolderError()
                }
            }
        }
    }


    /* Notify user that the selected folder does not contain any .mpa files */
    private fun showEmptyFolderError() {
        val anchorView: View? = activity?.findViewById(R.id.bottom_navigation_view)
        val contextView: View? = this.view?.rootView
        if (contextView != null && anchorView != null) {
            Snackbar.make(contextView, "The folder used for on-device maps must contain .map files.", Snackbar.LENGTH_INDEFINITE)
                .setAction("Dismiss") { }
                .setAnchorView(anchorView)
                .show()
        }
    }

}
