/*
 * TrackFragmentLayoutHolder.kt
 * Implements the TrackFragmentLayoutHolder class
 * A TrackFragmentLayoutHolder hold references to the main views of a track fragment
 *
 * This file is part of
 * TRACKBOOK - Movement Recorder for Android
 *
 * Copyright (c) 2016-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 *
 * Trackbook uses osmdroid - OpenStreetMap-Tools for Android
 * https://github.com/osmdroid/osmdroid
 */


package org.y20k.trackbook.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Group
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textview.MaterialTextView
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.osmdroid.api.IGeoPoint
import org.osmdroid.api.IMapController
import org.osmdroid.events.MapListener
import org.osmdroid.events.ScrollEvent
import org.osmdroid.events.ZoomEvent
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.OverlayItem
import org.osmdroid.views.overlay.Polyline
import org.osmdroid.views.overlay.TilesOverlay
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider
import org.y20k.trackbook.Keys
import org.y20k.trackbook.R
import org.y20k.trackbook.core.Track
import org.y20k.trackbook.helpers.AppThemeHelper
import org.y20k.trackbook.helpers.DateTimeHelper
import org.y20k.trackbook.helpers.FileHelper
import org.y20k.trackbook.helpers.LengthUnitHelper
import org.y20k.trackbook.helpers.MapHelper
import org.y20k.trackbook.helpers.MapOverlayHelper
import org.y20k.trackbook.helpers.PreferencesHelper
import kotlin.math.roundToInt


/*
 * TrackFragmentLayoutHolder class
 */
data class TrackFragmentLayoutHolder(private var activity: AppCompatActivity, private var tapListener: MapOverlayHelper.TapListener, private var inflater: LayoutInflater, private var container: ViewGroup?): MapListener {

    /* Define log tag */
    private val TAG: String = TrackFragmentLayoutHolder::class.java.simpleName


    /* Main class variables */
    val rootView: View
    var track: Track
    val shareButton: ImageButton
    val deleteButton: ImageButton
    val editButton: ImageButton
    val trackNameView: MaterialTextView
    val statisticsSheet: NestedScrollView
    private val mapView: MapView
    private lateinit var trackSpecialMarkersOverlay: ItemizedIconOverlay<OverlayItem>
    // private var trackOverlay: SimpleFastPointOverlay?
    private lateinit var trackOverlay: Polyline
    private var controller: IMapController
    // private var zoomLevel: Double
    private val statisticsSheetBehavior: BottomSheetBehavior<View>

    private val statisticsView: View
    private val distanceView: MaterialTextView
    private val stepsTitleView: MaterialTextView
    private val stepsView: MaterialTextView
    private val waypointsView: MaterialTextView
    private val durationView: MaterialTextView
    private val velocityView: MaterialTextView
    private val recordingStartView: MaterialTextView
    private val recordingStopView: MaterialTextView
    private val recordingPausedView: MaterialTextView
    private val recordingPausedLabelView: MaterialTextView
    private val maxAltitudeView: MaterialTextView
    private val minAltitudeView: MaterialTextView
    private val positiveElevationView: MaterialTextView
    private val negativeElevationView: MaterialTextView
    private val elevationDataViews: Group
    private val trackManagementViews: Group
    private val useImperialUnits: Boolean


    /* Init block */
    init {
        // find views
        rootView = inflater.inflate(R.layout.fragment_track, container, false)
        mapView = rootView.findViewById(R.id.map)
        shareButton = rootView.findViewById(R.id.save_button)
        deleteButton = rootView.findViewById(R.id.delete_button)
        editButton = rootView.findViewById(R.id.edit_button)
        trackNameView = rootView.findViewById(R.id.statistics_track_name_headline)

        // basic map setup
        track = Track()
        controller = mapView.controller
        setMapSource()
        mapView.addMapListener(this)
        mapView.isTilesScaledToDpi = true
        mapView.setMultiTouchControls(true)
        mapView.zoomController.setVisibility(org.osmdroid.views.CustomZoomButtonsController.Visibility.NEVER)
        controller.setZoom(Keys.DEFAULT_ZOOM_LEVEL)

        // get views for statistics sheet
        statisticsSheet = rootView.findViewById(R.id.statistics_sheet)
        statisticsView = rootView.findViewById(R.id.statistics_view)
        distanceView = rootView.findViewById(R.id.statistics_data_distance)
        stepsTitleView = rootView.findViewById(R.id.statistics_p_steps)
        stepsView = rootView.findViewById(R.id.statistics_data_steps)
        waypointsView = rootView.findViewById(R.id.statistics_data_waypoints)
        durationView = rootView.findViewById(R.id.statistics_data_duration)
        velocityView = rootView.findViewById(R.id.statistics_data_velocity)
        recordingStartView = rootView.findViewById(R.id.statistics_data_recording_start)
        recordingStopView = rootView.findViewById(R.id.statistics_data_recording_stop)
        recordingPausedLabelView = rootView.findViewById(R.id.statistics_p_recording_paused)
        recordingPausedView = rootView.findViewById(R.id.statistics_data_recording_paused)
        maxAltitudeView = rootView.findViewById(R.id.statistics_data_max_altitude)
        minAltitudeView = rootView.findViewById(R.id.statistics_data_min_altitude)
        positiveElevationView = rootView.findViewById(R.id.statistics_data_positive_elevation)
        negativeElevationView = rootView.findViewById(R.id.statistics_data_negative_elevation)
        elevationDataViews = rootView.findViewById(R.id.elevation_data)
        trackManagementViews = rootView.findViewById(R.id.management_icons)

        // get measurement unit system
        useImperialUnits = PreferencesHelper.loadUseImperialUnits()

        // set dark map tiles, if necessary
        if (AppThemeHelper.isDarkModeOn(activity)) {
            mapView.overlayManager.tilesOverlay.setColorFilter(TilesOverlay.INVERT_COLORS)
        }

        // add compass to map
        val compassOverlay = CompassOverlay(activity, InternalCompassOrientationProvider(activity), mapView)
        compassOverlay.enableCompass()
        compassOverlay.setCompassCenter(36f, 36f)
//        compassOverlay.setCompassCenter(36f, 36f + (statusBarHeight / densityScalingFactor)) // TODO uncomment when transparent status bar is re-implemented
        mapView.overlays.add(compassOverlay)

//        // create map overlay
//        val mapOverlayHelper: MapOverlayHelper = MapOverlayHelper(markerListener)
//        trackOverlay = mapOverlayHelper.createTrackOverlay(context, track, Keys.STATE_TRACKING_NOT)
//        trackSpecialMarkersOverlay = mapOverlayHelper.createSpecialMakersTrackOverlay(context, track, Keys.STATE_TRACKING_NOT, displayStartEndMarker = true)
//        if (track.wayPoints.isNotEmpty()) {
//            mapView.overlays.add(trackSpecialMarkersOverlay)
//            mapView.overlays.add(trackOverlay)
//        }

        // basic set up of statistics sheet
        statisticsSheet.background.alpha = 233 /* of 255 */
        statisticsSheetBehavior = BottomSheetBehavior.from<View>(statisticsSheet)
        statisticsSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        statisticsSheetBehavior.addBottomSheetCallback(getStatisticsSheetCallback())
    }


    /* Set map source */
    private fun setMapSource() {
        when (PreferencesHelper.loadOnDeviceMaps()) {
            // CASE: Trackbook will use offline maps (on-device)
            true -> {
                val mapFiles: List<DocumentFile> = FileHelper.getOnDeviceMapFiles(activity)
                if (mapFiles.isNotEmpty()) {
                    mapView.setTileProvider(MapHelper.getOfflineMapProvider(activity, mapFiles))
                    mapView.isTilesScaledToDpi = false
                } else {
                    // fallback to use online maps, when no map files are available
                    mapView.setTileSource(TileSourceFactory.MAPNIK)
                    mapView.isTilesScaledToDpi = true
                    Toast.makeText(activity, R.string.toast_message_no_map_files_available, Toast.LENGTH_LONG).show()
                }
            }
            // CASE: Trackbook will use online maps (OSM)
            false -> {
                mapView.setTileSource(TileSourceFactory.MAPNIK)
                mapView.isTilesScaledToDpi = true
            }
        }
    }


    /* Updates map overlay */
    fun updateTrackOverlay(centerMap: Boolean, updateStatisticsSheet: Boolean, saveTrack: Boolean) {
        if (this::trackOverlay.isInitialized) {
            mapView.overlays.remove(trackOverlay)
        }
        if (this::trackSpecialMarkersOverlay.isInitialized) {
            mapView.overlays.remove(trackSpecialMarkersOverlay)
        }
        if (track.wayPoints.isNotEmpty()) {
            val mapOverlayHelper: MapOverlayHelper = MapOverlayHelper(tapListener)
            trackOverlay = mapOverlayHelper.createTrackOverlay(activity, track, Keys.STATE_TRACKING_NOT)
            trackSpecialMarkersOverlay = mapOverlayHelper.createSpecialMakersTrackOverlay(activity, track, Keys.STATE_TRACKING_NOT, displayStartEndMarker = true)
            mapView.overlays.add(trackOverlay)
            mapView.overlays.add(trackSpecialMarkersOverlay)
        }
        if (centerMap) {
            controller.setCenter(GeoPoint(track.latitude, track.longitude))
            controller.setZoom(track.zoomLevel)
        }
        if (updateStatisticsSheet) {
            setupStatisticsViews()
        }
        if (saveTrack) {
            activity.lifecycleScope.launch {
                withContext(IO) {
                    FileHelper.saveTrack(track, true)
                }
            }
        }
    }


    /* Saves zoom level and center of this map */
    fun saveViewStateToTrack() {
        if (track.latitude != 0.0 && track.longitude != 0.0) {
            activity.lifecycleScope.launch {
                withContext(IO) {
                    FileHelper.saveTrack(track, false)
                }
            }
        }
    }


    /* Sets up the statistics sheet */
    private fun setupStatisticsViews() {

        // get step count string - hide step count if not available
        val steps: String
        if (track.stepCount == -1f) {
            steps = activity.getString(R.string.statistics_sheet_p_steps_no_pedometer)
            stepsTitleView.isGone = true
            stepsView.isGone = true
        }
        else {
            steps = track.stepCount.roundToInt().toString()
            stepsTitleView.isVisible = true
            stepsView.isVisible = true
        }

        // populate views
        trackNameView.text = track.name
        distanceView.text = LengthUnitHelper.convertDistanceToString(track.length, useImperialUnits)
        stepsView.text = steps
        waypointsView.text = track.wayPoints.size.toString()
        durationView.text = DateTimeHelper.convertToReadableTime(activity, track.duration)
        velocityView.text = LengthUnitHelper.convertToVelocityString(track.duration, track.recordingPaused, track.length, useImperialUnits)
        recordingStartView.text = DateTimeHelper.convertToReadableDateAndTime(track.recordingStart)
        recordingStopView.text = DateTimeHelper.convertToReadableDateAndTime(track.recordingStop)
        maxAltitudeView.text = LengthUnitHelper.convertDistanceToString(track.maxAltitude, useImperialUnits)
        minAltitudeView.text = LengthUnitHelper.convertDistanceToString(track.minAltitude, useImperialUnits)
        positiveElevationView.text = LengthUnitHelper.convertDistanceToString(track.positiveElevation, useImperialUnits)
        negativeElevationView.text = LengthUnitHelper.convertDistanceToString(track.negativeElevation, useImperialUnits)

        // show / hide recording pause
        if (track.recordingPaused != 0L) {
            recordingPausedLabelView.isVisible = true
            recordingPausedView.isVisible = true
            recordingPausedView.text = DateTimeHelper.convertToReadableTime(activity, track.recordingPaused)
        } else {
            recordingPausedLabelView.isGone = true
            recordingPausedView.isGone = true
        }

        // inform user about possible accuracy issues with altitude measurements
        elevationDataViews.referencedIds.forEach { id ->
            (rootView.findViewById(id) as View).setOnClickListener{
                Toast.makeText(activity, R.string.toast_message_elevation_info, Toast.LENGTH_LONG).show()
            }
        }
        // make track name on statistics sheet clickable
        trackNameView.setOnClickListener {
            toggleStatisticsSheetVisibility()
        }
    }


    /* Shows/hides the statistics sheet */
    private fun toggleStatisticsSheetVisibility() {
        when (statisticsSheetBehavior.state) {
            BottomSheetBehavior.STATE_EXPANDED -> statisticsSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            else -> statisticsSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }


    /* Defines the behavior of the statistics sheet  */
    private fun getStatisticsSheetCallback(): BottomSheetBehavior.BottomSheetCallback {
        return object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        trackManagementViews.isVisible = true
                        shareButton.isGone = true
                        // bottomSheet.setPadding(0,24,0,0)
                    }
                    else -> {
                        trackManagementViews.isGone = true
                        shareButton.isVisible = true
                        // bottomSheet.setPadding(0,0,0,0)
                    }
                }
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (slideOffset < 0.125f) {
                    trackManagementViews.isGone = true
                    shareButton.isVisible = true
                } else {
                    trackManagementViews.isVisible = true
                    shareButton.isGone = true
                }
            }
        }
    }


    /* Overrides onZoom from MapListener */
    override fun onZoom(event: ZoomEvent?): Boolean {
        if (event == null) {
            return false
        } else {
            track.zoomLevel = event.zoomLevel
            return true
        }
    }


    /* Overrides onScroll from MapListener */
    override fun onScroll(event: ScrollEvent?): Boolean {
        if (event == null) {
            return false
        } else {
            val center: IGeoPoint = mapView.mapCenter
            track.latitude = center.latitude
            track.longitude = center.longitude
            return true
        }
    }

}

