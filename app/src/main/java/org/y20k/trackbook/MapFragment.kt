/*
 * MapFragment.kt
 * Implements the MapFragment fragment
 * A MapFragment displays a map using osmdroid as well as the controls to start / stop a recording
 *
 * This file is part of
 * TRACKBOOK - Movement Recorder for Android
 *
 * Copyright (c) 2016-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 *
 * Trackbook uses osmdroid - OpenStreetMap-Tools for Android
 * https://github.com/osmdroid/osmdroid
 */


package org.y20k.trackbook

import YesNoDialog
import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.y20k.trackbook.core.Track
import org.y20k.trackbook.core.TracklistElement
import org.y20k.trackbook.core.WayPoint
import org.y20k.trackbook.helpers.FileHelper
import org.y20k.trackbook.helpers.LocationHelper
import org.y20k.trackbook.helpers.MapOverlayHelper
import org.y20k.trackbook.helpers.PreferencesHelper
import org.y20k.trackbook.helpers.TrackHelper
import org.y20k.trackbook.helpers.UiHelper
import org.y20k.trackbook.ui.MapFragmentLayoutHolder


/*
 * MapFragment class
 */
class MapFragment : Fragment(), YesNoDialog.YesNoDialogListener, MapOverlayHelper.TapListener {

    /* Define log tag */
    private val TAG: String = MapFragment::class.java.simpleName


    /* Main class variables */
    private var bound: Boolean = false
    private val handler: Handler = Handler(Looper.getMainLooper())
    private var trackingState: Int = Keys.STATE_TRACKING_NOT
    private var gpsProviderActive: Boolean = false
    private var networkProviderActive: Boolean = false
    private var track: Track = Track()
    private lateinit var currentBestLocation: Location
    private lateinit var layout: MapFragmentLayoutHolder
    private lateinit var trackerService: TrackerService


    /* Overrides onCreate from Fragment */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO make only MapFragment's status bar transparent - see: https://gist.github.com/Dvik/a3de88d39da9d1d6d175025a56c5e797#file-viewextension-kt and https://proandroiddev.com/android-full-screen-ui-with-transparent-status-bar-ef52f3adde63
        // get current best location
        currentBestLocation = LocationHelper.getLastKnownLocation(activity as Context)
        // get saved tracking state
        trackingState = PreferencesHelper.loadTrackingState()
    }


    /* Overrides onStop from Fragment */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // initialize layout
        val statusBarHeight: Int = UiHelper.getStatusBarHeight(activity as Context)
        layout = MapFragmentLayoutHolder(activity as Context, this as MapOverlayHelper.TapListener, inflater, container, statusBarHeight, currentBestLocation, trackingState)

        // set up buttons
        layout.currentLocationButton.setOnClickListener {
            layout.centerMap(currentBestLocation, animated = true)
        }
        layout.mainButton.setOnClickListener {
            handleTrackingManagementMenu()
        }
        layout.saveButton.setOnClickListener {
            saveTrack()
        }
        layout.clearButton.setOnClickListener {
            if (track.wayPoints.isNotEmpty()) {
                YesNoDialog(this as YesNoDialog.YesNoDialogListener).show(context = activity as Context, type = Keys.DIALOG_DELETE_CURRENT_RECORDING, message = R.string.dialog_delete_current_recording_message, yesButton = R.string.dialog_delete_current_recording_button_discard)
            } else {
                trackerService.clearTrack()
            }
        }

        return layout.rootView
    }


    /* Overrides onStart from Fragment */
    override fun onStart() {
        super.onStart()
        // request location permission if denied
        if (ContextCompat.checkSelfPermission(activity as Context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            requestLocationPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        // bind to TrackerService
        activity?.bindService(Intent(activity, TrackerService::class.java), connection, Context.BIND_AUTO_CREATE)
    }


    /* Overrides onPause from Fragment */
    override fun onPause() {
        super.onPause()
        layout.saveState(currentBestLocation)
        if (bound && trackingState != Keys.STATE_TRACKING_ACTIVE) {
            trackerService.removeGpsLocationListener()
            trackerService.removeNetworkLocationListener()
        }
    }


    /* Overrides onStop from Fragment */
    override fun onStop() {
        super.onStop()
        // unbind from TrackerService
        activity?.unbindService(connection)
        handleServiceUnbind()
    }


    /* Overrides onYesNoDialog from YesNoDialogListener */
    override fun onYesNoDialog(type: Int, dialogResult: Boolean, payload: Int, payloadString: String) {
        super.onYesNoDialog(type, dialogResult, payload, payloadString)
        when (type) {
            Keys.DIALOG_EMPTY_RECORDING -> {
                if (dialogResult) {
                    // user tapped resume
                    trackerService.resumeTracking()
                }
            }
            Keys.DIALOG_DELETE_CURRENT_RECORDING -> {
                if (dialogResult) {
                    trackerService.clearTrack()
                }
            }
        }
    }


    /* Overrides onTapped from TapListener */
    override fun onTapped(latitude: Double, longitude: Double, type: Int) {
        if (bound) {
            val wayPoint: WayPoint? = TrackHelper.getClosestWaypoint(track, latitude, longitude)
            if (wayPoint != null) {
                // create snackbar content
                val snackbarMessage: String = TrackHelper.getWaypointDescription(requireContext(), wayPoint.accuracy, wayPoint.provider, wayPoint.time)
                val snackbarActionTitle: Int
                if (wayPoint.starred) snackbarActionTitle = R.string.snackbar_message_button_remove_favorite
                else snackbarActionTitle = R.string.snackbar_message_button_add_favorite
                // setup and show snackbar
                Snackbar.make(layout.rootView, snackbarMessage, Snackbar.LENGTH_SHORT)
                    .setAnchorView(layout.mainButton)
                    .setAction(snackbarActionTitle) {
                        // user selected Save or Remove: update track display
                        when (type) {
                            Keys.TYPE_MARKER -> {
                                track = TrackHelper.toggleStarred(track, latitude, longitude)
                                layout.overlayCurrentTrack(track, trackingState)
                                trackerService.track = track
                            }
                            Keys.TYPE_POLYLINE -> {
                                track = TrackHelper.toggleClosestStarred(track, latitude, longitude)
                                layout.overlayCurrentTrack(track, trackingState)
                                trackerService.track = track
                            }
                        }
                    }
                    .show()
            }
        }
    }


    /* Start / resume recording waypoints */
    private fun startTracking(resume: Boolean) {
        if (ContextCompat.checkSelfPermission(activity as Context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                recordingPermissionsResultLauncher.launch(arrayOf(Manifest.permission.ACTIVITY_RECOGNITION, Manifest.permission.POST_NOTIFICATIONS))
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                recordingPermissionsResultLauncher.launch(arrayOf(Manifest.permission.ACTIVITY_RECOGNITION))
            } else {
                // start tracker service and start recording
                startTrackerService(resume = resume)
            }
        }
    }


    /* Start tracker service and start or resume tracking */
    private fun startTrackerService(resume: Boolean = false) {
        val intent = Intent(activity, TrackerService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // ... start service in foreground to prevent it being killed on Oreo
            activity?.startForegroundService(intent)
        } else {
            activity?.startService(intent)
        }
        if (resume) {
            trackerService.resumeTracking()
        } else {
            trackerService.startTracking()
        }
    }


    /* Register permission launcher for requesting location */
    private val requestLocationPermissionLauncher = registerForActivityResult(RequestPermission()) { isGranted: Boolean ->
        if (isGranted) {
            // permission was granted - re-bind service
            if (bound) activity?.unbindService(connection)
            activity?.bindService(Intent(activity, TrackerService::class.java),  connection,  Context.BIND_AUTO_CREATE)
            Log.i(TAG, "Request result: Location permission has been granted.")
        } else {
            // permission denied - unbind service
            if (bound) activity?.unbindService(connection)
        }
        layout.toggleLocationErrorBar(gpsProviderActive, networkProviderActive)
    }


    /* Register permission launcher for starting/resuming a recording via the tracker service */
    private val recordingPermissionsResultLauncher: ActivityResultLauncher<Array<String>> = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissionsStatusMap ->
        val locationPermissionGranted: Boolean = ContextCompat.checkSelfPermission(activity as Context, Manifest.permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED
        // check if at least POST_NOTIFICATIONS is granted
        if (permissionsStatusMap.getOrDefault(Manifest.permission.POST_NOTIFICATIONS, true)) {
            if (locationPermissionGranted) {
                // start tracker service and start / resume recording
                startTrackerService(resume = trackingState == Keys.STATE_TRACKING_PAUSED)
            }
        }
        // inform user about missing permissions
        if (!locationPermissionGranted && !permissionsStatusMap.getOrDefault(Manifest.permission.POST_NOTIFICATIONS, true)) {
            Toast.makeText(activity, R.string.toast_message_error_permission_notification_denied, Toast.LENGTH_LONG).show()
        }
        if (locationPermissionGranted && !permissionsStatusMap.getOrDefault(Manifest.permission.ACTIVITY_RECOGNITION, true)) {
            Toast.makeText(activity, R.string.toast_message_error_permission_activity_recognition_denied, Toast.LENGTH_LONG).show()
        }
    }


    /* Handles state when service is being unbound */
    private fun handleServiceUnbind() {
        bound = false
        // unregister listener for changes in shared preferences
        PreferencesHelper.unregisterPreferenceChangeListener(sharedPreferenceChangeListener)
        // stop receiving location updates
        handler.removeCallbacks(periodicLocationRequestRunnable)
    }



    /* Starts / pauses tracking and toggles the recording sub menu_bottom_navigation */
    private fun handleTrackingManagementMenu() {
        when (trackingState) {
            Keys.STATE_TRACKING_PAUSED -> startTracking(resume = true)
            Keys.STATE_TRACKING_ACTIVE -> trackerService.stopTracking()
            Keys.STATE_TRACKING_NOT -> startTracking(resume = false)
        }
    }


    /* Saves track - shows dialog, if recording is still empty */
    private fun saveTrack() {
        if (track.wayPoints.isEmpty()) {
            YesNoDialog(this as YesNoDialog.YesNoDialogListener).show(context = activity as Context, type = Keys.DIALOG_EMPTY_RECORDING, message = R.string.dialog_error_empty_recording_message, yesButton = R.string.dialog_error_empty_recording_button_resume)
        } else {
            lifecycleScope.launch {
                withContext(IO) {
                    // step 1: create and store filenames for json and gpx files
                    track.trackUriString = FileHelper.getTrackFileUri(activity as Context, track).toString()
                    track.gpxUriString = FileHelper.getGpxFileUri(activity as Context, track).toString()
                    // step 2: save track
                    FileHelper.saveTrack(track, saveGpxToo = true)
                    // step 3: save tracklist
                    FileHelper.addTrackAndSaveTracklist(activity as Context, track)
                    // step 3: clear track
                    trackerService.clearTrack()
                }
                // step 4: open track in TrackFragment
                openTrack(track.toTracklistElement(activity as Context))
            }
        }
    }


    /* Opens a track in TrackFragment */
    private fun openTrack(tracklistElement: TracklistElement) {
        val bundle: Bundle = Bundle()
        bundle.putString(Keys.ARG_TRACK_TITLE, tracklistElement.name)
        bundle.putString(Keys.ARG_TRACK_FILE_URI, tracklistElement.trackUriString)
        bundle.putString(Keys.ARG_GPX_FILE_URI, tracklistElement.gpxUriString)
        bundle.putLong(Keys.ARG_TRACK_ID, tracklistElement.getTrackId())
        val options: NavOptions = NavOptions.Builder().setPopUpTo(R.id.map_fragment, true).build()
        findNavController().navigate(R.id.action_map_fragment_to_track_fragment, bundle, options)
    }


    /*
     * Defines the listener for changes in shared preferences
     */
    private val sharedPreferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        when (key) {
            Keys.PREF_TRACKING_STATE -> {
                if (activity != null) {
                    trackingState = PreferencesHelper.loadTrackingState()
                    layout.updateMainButton(trackingState)
                }
            }
        }
    }
    /*
     * End of declaration
     */


    /*
     * Defines callbacks for service binding, passed to bindService()
     */
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            bound = true
            // get reference to tracker service
            val binder = service as TrackerService.LocalBinder
            trackerService = binder.service
            // get state of tracking and update button if necessary
            trackingState = trackerService.trackingState
            layout.updateMainButton(trackingState)
            // register listener for changes in shared preferences
            PreferencesHelper.registerPreferenceChangeListener(sharedPreferenceChangeListener)
            // start listening for location updates
            handler.removeCallbacks(periodicLocationRequestRunnable)
            handler.postDelayed(periodicLocationRequestRunnable, 0)
        }
        override fun onServiceDisconnected(arg0: ComponentName) {
            // service has crashed, or was killed by the system
            handleServiceUnbind()
        }
    }
    /*
     * End of declaration
     */


    /*
     * Runnable: Periodically requests location
     */
    private val periodicLocationRequestRunnable: Runnable = object : Runnable {
        override fun run() {
            // pull current state from service
            currentBestLocation = trackerService.currentBestLocation
            track = trackerService.track
            gpsProviderActive = trackerService.gpsProviderActive
            networkProviderActive = trackerService.networkProviderActive
            trackingState = trackerService.trackingState
            // update location and track
            layout.markCurrentPosition(currentBestLocation, trackingState)
            layout.overlayCurrentTrack(track, trackingState)
            layout.updateLiveStatics(length = track.length, duration = track.duration, trackingState = trackingState)
            // center map, if it had not been dragged/zoomed before
            if (!layout.userInteraction) { layout.centerMap(currentBestLocation, true)}
            // show error snackbar if necessary
            layout.toggleLocationErrorBar(gpsProviderActive, networkProviderActive)
            // use the handler to start runnable again after specified delay
            handler.postDelayed(this, Keys.REQUEST_CURRENT_LOCATION_INTERVAL)
        }
    }
    /*
     * End of declaration
     */

}
