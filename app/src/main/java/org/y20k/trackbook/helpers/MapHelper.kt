/*
 * MapHelper.kt
 * Implements the MapHelper object
 * A MapHelper offers helper methods for dealing with maps
 *
 * This file is part of
 * TRACKBOOK - Movement Recorder for Android
 *
 * Copyright (c) 2016-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 *
 * Trackbook uses osmdroid - OpenStreetMap-Tools for Android
 * https://github.com/osmdroid/osmdroid
 */


package org.y20k.trackbook.helpers

import android.content.Context
import androidx.documentfile.provider.DocumentFile
import org.mapsforge.map.android.rendertheme.AssetsRenderTheme
import org.mapsforge.map.rendertheme.XmlRenderTheme
import org.osmdroid.mapsforge.MapsForgeTileProvider
import org.osmdroid.mapsforge.MapsForgeTileSource
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver
import java.io.FileInputStream


/*
 * MapHelper object
 */
object MapHelper {

    /* Define log tag */
    private val TAG: String = MapHelper::class.java.simpleName


    /* Get a tile provider for offline maps */
    fun getOfflineMapProvider(context: Context, mapFiles: List<DocumentFile>): MapsForgeTileProvider {
        // load the render theme from the assets directory
        var theme: XmlRenderTheme? = null
        try {
            theme = AssetsRenderTheme(context.assets,"rendertheme/", "freizeitkarte-v5.xml")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // convert map files to input streams
        val mapFileInputStreams: Array<FileInputStream> = FileHelper.getOnDeviceMapFileInputStreams(context, mapFiles)
        // todo: test with a broken .map file
        // create and the the map tile provider
        val mapsForgeTileSource: MapsForgeTileSource = MapsForgeTileSource.createFromFileInputStream(mapFileInputStreams, theme, "freizeitkarte-v5.xml")
        return MapsForgeTileProvider(SimpleRegisterReceiver(context),mapsForgeTileSource, null)
    }

}