/*
 * NotificationHelper.kt
 * Implements the NotificationHelper class
 * A NotificationHelper creates and configures a notification
 *
 * This file is part of
 * TRACKBOOK - Movement Recorder for Android
 *
 * Copyright (c) 2016-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 *
 * Trackbook uses osmdroid - OpenStreetMap-Tools for Android
 * https://github.com/osmdroid/osmdroid
 */


package org.y20k.trackbook.helpers

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.NotificationCompat
import androidx.core.graphics.drawable.toBitmap
import org.y20k.trackbook.Keys
import org.y20k.trackbook.MainActivity
import org.y20k.trackbook.R
import org.y20k.trackbook.TrackerService


/*
 * NotificationHelper class
 */
class NotificationHelper(private val context: Context, private val notificationBuilder: NotificationCompat.Builder, private val notificationManager: NotificationManager) {

    /* Define log tag */
    private val TAG: String = NotificationHelper::class.java.simpleName


    /* Creates notification */
    fun createNotification(trackingState: Int, trackLength: Float, duration: Long, useImperial: Boolean): Notification {

        // create notification channel if necessary
        if (shouldCreateNotificationChannel()) {
            createNotificationChannel()
        }

        // Build notification
        notificationBuilder.setContentIntent(showActionPendingIntent)
        notificationBuilder.setSmallIcon(R.drawable.ic_notification_icon_small_24dp)
        notificationBuilder.setContentText(getContentString(context, duration, trackLength, useImperial))

        // add icon and actions for stop, resume and show
        when (trackingState) {
            Keys.STATE_TRACKING_ACTIVE -> {
                notificationBuilder.setContentTitle(context.getString(R.string.notification_title_trackbook_running))
                notificationBuilder.clearActions()
                notificationBuilder.addAction(stopAction)
                notificationBuilder.setLargeIcon(AppCompatResources.getDrawable(context, R.drawable.ic_notification_icon_large_tracking_active_48dp)!!.toBitmap())
            }
            else -> {
                notificationBuilder.setContentTitle(context.getString(R.string.notification_title_trackbook_not_running))
                notificationBuilder.clearActions()
                notificationBuilder.addAction(resumeAction)
                notificationBuilder.addAction(showAction)
                notificationBuilder.setLargeIcon(AppCompatResources.getDrawable(context, R.drawable.ic_notification_icon_large_tracking_stopped_48dp)!!.toBitmap())
            }
        }

        return notificationBuilder.build()

    }


    /* Build context text for notification builder */
    private fun getContentString(context: Context, duration: Long, trackLength: Float, useImperial: Boolean): String {
        return "${LengthUnitHelper.convertDistanceToString(trackLength, useImperial)} • ${DateTimeHelper.convertToReadableTime(context, duration)}"
    }


    /* Checks if notification channel should be created */
    private fun shouldCreateNotificationChannel() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && !nowPlayingChannelExists()


    /* Checks if notification channel exists */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun nowPlayingChannelExists() = notificationManager.getNotificationChannel(Keys.NOTIFICATION_CHANNEL_RECORDING) != null


    /* Create a notification channel */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val notificationChannel = NotificationChannel(Keys.NOTIFICATION_CHANNEL_RECORDING,
            context.getString(R.string.notification_channel_recording_name),
            NotificationManager.IMPORTANCE_LOW)
            .apply { description = context.getString(R.string.notification_channel_recording_description) }
        notificationManager.createNotificationChannel(notificationChannel)
    }


    /* Notification pending intents */
    private val stopActionPendingIntent = PendingIntent.getService(
        context,14,
        Intent(context, TrackerService::class.java).setAction(Keys.ACTION_STOP),PendingIntent.FLAG_IMMUTABLE)
    private val resumeActionPendingIntent = PendingIntent.getService(
        context, 16,
        Intent(context, TrackerService::class.java).setAction(Keys.ACTION_RESUME),PendingIntent.FLAG_IMMUTABLE)
    private val showActionPendingIntent: PendingIntent? = TaskStackBuilder.create(context).run {
        addNextIntentWithParentStack(Intent(context, MainActivity::class.java))
        getPendingIntent(10, PendingIntent.FLAG_IMMUTABLE)
    }


    /* Notification actions */
    private val stopAction = NotificationCompat.Action(
        R.drawable.ic_notification_action_stop_24dp,
        context.getString(R.string.notification_pause),
        stopActionPendingIntent)
    private val resumeAction = NotificationCompat.Action(
        R.drawable.ic_notification_action_resume_36dp,
        context.getString(R.string.notification_resume),
        resumeActionPendingIntent)
    private val showAction = NotificationCompat.Action(
        R.drawable.ic_notification_action_show_36dp,
        context.getString(R.string.notification_show),
        showActionPendingIntent)

}