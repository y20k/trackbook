/*
 * FileHelper.kt
 * Implements the FileHelper object
 * A FileHelper provides helper methods for reading and writing files from and to device storage
 *
 * This file is part of
 * TRACKBOOK - Movement Recorder for Android
 *
 * Copyright (c) 2016-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 *
 * Trackbook uses osmdroid - OpenStreetMap-Tools for Android
 * https://github.com/osmdroid/osmdroid
 */


package org.y20k.trackbook.helpers

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Log
import androidx.core.net.toFile
import androidx.core.net.toUri
import androidx.documentfile.provider.DocumentFile
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.y20k.trackbook.Keys
import org.y20k.trackbook.core.Track
import org.y20k.trackbook.core.Tracklist
import org.y20k.trackbook.core.TracklistElement
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.io.InputStreamReader
import java.text.NumberFormat
import java.util.Date
import java.util.GregorianCalendar
import kotlin.math.ln
import kotlin.math.pow


/*
 * FileHelper object
 */
object FileHelper {

    /* Define log tag */
    private val TAG: String = FileHelper::class.java.simpleName


    /* Return an InputStream for given Uri */
    fun getTextFileStream(context: Context, uri: Uri): InputStream? {
        var stream : InputStream? = null
        try {
            stream = context.contentResolver.openInputStream(uri)
        } catch (e : Exception) {
            e.printStackTrace()
        }
        return stream
    }


    /* Get file size for given Uri */
    fun getFileSize(context: Context, uri: Uri): Long {
        val cursor: Cursor? = context.contentResolver.query(uri, null, null, null, null)
        if (cursor != null) {
            val sizeIndex: Int = cursor.getColumnIndex(OpenableColumns.SIZE)
            cursor.moveToFirst()
            val size: Long = cursor.getLong(sizeIndex)
            cursor.close()
            return size
        } else {
            return 0L
        }
    }


    /* Get file name for given Uri */
    fun getFileName(context: Context, uri: Uri): String {
        val cursor: Cursor? = context.contentResolver.query(uri, null, null, null, null)
        if (cursor != null) {
            val nameIndex: Int = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            cursor.moveToFirst()
            val name: String = cursor.getString(nameIndex)
            cursor.close()
            return name
        } else {
            return String()
        }
    }


    /* make a content uri persistent / claims the uri */
    fun makeUriPersistent(context: Context, uri: Uri) {
        val contentResolver = context.contentResolver
        val takeFlags: Int = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        return contentResolver.takePersistableUriPermission(uri, takeFlags)
    }


    /* Clears given folder - keeps given number of files */
    fun clearFolder(folder: File?, keep: Int, deleteFolder: Boolean = false) {
        if (folder != null && folder.exists()) {
            val files = folder.listFiles()
            val fileCount: Int = files.size
            files.sortBy { it.lastModified() }
            for (fileNumber in files.indices) {
                if (fileNumber < fileCount - keep) {
                    files[fileNumber].delete()
                }
            }
            if (deleteFolder && keep == 0) {
                folder.delete()
            }
        }
    }


    /* Reads tracklist from storage using GSON */
    fun readTracklist(context: Context): Tracklist {
        Log.v(TAG, "Reading Tracklist - Thread: ${Thread.currentThread().name}")
        // get JSON from text file
        val json: String = readTextFile(context, getTracklistFileUri(context))
        var tracklist: Tracklist = Tracklist()
        if (json.isNotBlank()) {
            try {
                tracklist = getCustomGson().fromJson(json, Tracklist::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return tracklist
    }


    /* Reads track from storage using GSON */
    fun readTrack(context: Context, fileUri: Uri): Track {
        // get JSON from text file
        val json: String = readTextFile(context, fileUri)
        var track: Track = Track()
        if (json.isNotEmpty()) {
            // convert JSON and return as track
            try {
                track = getCustomGson().fromJson(json, Track::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return track
    }


    /* Deletes temp track file */
    fun deleteTempFile(context: Context) {
        getTempFileUri(context).toFile().delete()
    }


    /* Checks if temp track file exists */
    fun tempFileExists(context: Context): Boolean {
        return getTempFileUri(context).toFile().exists()
    }


    /* Creates Uri for Gpx file of a track */
    fun getGpxFileUri(context: Context, track: Track): Uri = File(context.getExternalFilesDir(Keys.FOLDER_GPX), getGpxFileName(track)).toUri()


    /* Creates file name for Gpx file of a track */
    fun getGpxFileName(track: Track): String = DateTimeHelper.convertToSortableDateString(track.recordingStart) + Keys.GPX_FILE_EXTENSION


    /* Creates Uri for json track file */
    fun getTrackFileUri(context: Context, track: Track): Uri {
        val fileName: String = DateTimeHelper.convertToSortableDateString(track.recordingStart) + Keys.TRACKBOOK_FILE_EXTENSION
        return File(context.getExternalFilesDir(Keys.FOLDER_TRACKS), fileName).toUri()
    }


    /* Creates Uri for json temp track file */
    fun getTempFileUri(context: Context): Uri {
        return File(context.getExternalFilesDir(Keys.FOLDER_TEMP), Keys.TEMP_FILE).toUri()
    }


    /* Adds a track and saves the tracklist */
    fun addTrackAndSaveTracklist(context: Context, track: Track, modificationDate: Date = track.recordingStop) {
        val tracklist: Tracklist = readTracklist(context)
        tracklist.tracklistElements.add(track.toTracklistElement(context))
        tracklist.totalDistanceAll += track.length
//            tracklist.totalDurationAll += track.duration // note: TracklistElement does not contain duration
//            tracklist.totalRecordingPausedAll += track.recordingPaused // note: TracklistElement does not contain recordingPaused
//            tracklist.totalStepCountAll += track.stepCount // note: TracklistElement does not contain stepCount
        return saveTracklist(context, tracklist, modificationDate)
    }


    /* Deletes tracks that are not starred */
    fun deleteNonStarred(context: Context, tracklist: Tracklist): Tracklist {
        val elementsToRemove = mutableListOf<TracklistElement>()
        tracklist.tracklistElements.forEach { tracklistElement ->
            if (!tracklistElement.starred) {
                elementsToRemove.add(tracklistElement)
            }
        }
        elementsToRemove.forEach { tracklistElement ->
            // delete track files
            tracklistElement.trackUriString.toUri().toFile().delete()
            tracklistElement.gpxUriString.toUri().toFile().delete()
            // subtract track length from total distance
            tracklist.totalDistanceAll -= tracklistElement.length
        }
        // delete tracks from tracklist
        tracklist.tracklistElements.removeAll{ elementsToRemove.contains(it) }
        saveTracklist(context, tracklist, GregorianCalendar.getInstance().time)
        Log.e(TAG, "DING => ${tracklist.totalDistanceAll}")
        return tracklist
    }


    /* Save Track as JSON to storage */
    fun saveTrack(track: Track, saveGpxToo: Boolean) {
        val jsonString: String = getTrackJsonString(track)
        if (jsonString.isNotBlank()) {
            // write track file
            writeTextFile(jsonString, track.trackUriString.toUri())
        }
        if (saveGpxToo) {
            val gpxString: String = TrackHelper.createGpxString(track)
            if (gpxString.isNotBlank()) {
                // write GPX file
                writeTextFile(gpxString, track.gpxUriString.toUri())
            }
        }
    }


    /* Save Temp Track as JSON to storage */
    fun saveTempTrack(context: Context, track: Track) {
        val json: String = getTrackJsonString(track)
        if (json.isNotBlank()) {
            writeTextFile(json, getTempFileUri(context))
        }
    }


    /* Saves track tracklist as JSON text file */
    fun saveTracklist(context: Context, tracklist: Tracklist, modificationDate: Date) {
        tracklist.modificationDate = modificationDate
        // convert to JSON
        val gson: Gson = getCustomGson()
        var json: String = String()
        try {
            json = gson.toJson(tracklist)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (json.isNotBlank()) {
            // write text file
            writeTextFile(json, getTracklistFileUri(context))
        }
    }


    /* Renames track */
    fun renameTrack(context: Context, track: Track, newName: String) {
        // search track in tracklist
        val tracklist: Tracklist = readTracklist(context)
        var trackUriString: String = String()
        tracklist.tracklistElements.forEach { tracklistElement ->
            if (tracklistElement.getTrackId() == track.getTrackId()) {
                // rename tracklist element
                Log.e(TAG, "DING") // todo remove
                tracklistElement.name = newName
                trackUriString = tracklistElement.trackUriString
            }
        }
        if (trackUriString.isNotEmpty()) {
            // save tracklist
            saveTracklist(context, tracklist, GregorianCalendar.getInstance().time)
            // rename track
            track.name = newName
            // save track
            saveTrack(track, saveGpxToo = true)
        }
    }


    /* Deletes one track */
    fun deleteTrack(context: Context, position: Int, tracklist: Tracklist): Tracklist {
        val tracklistElement: TracklistElement = tracklist.tracklistElements[position]
        // delete track files
        tracklistElement.trackUriString.toUri().toFile().delete()
        tracklistElement.gpxUriString.toUri().toFile().delete()
        // subtract track length from total distance
        tracklist.totalDistanceAll -= tracklistElement.length
        // remove track element from list
        tracklist.tracklistElements.removeIf { it.getTrackId() == tracklistElement.getTrackId() }
        saveTracklist(context, tracklist, GregorianCalendar.getInstance().time)
        return tracklist
    }


    /* Get a list of .map files stored on device */
    fun getOnDeviceMapFiles(context: Context): List<DocumentFile> {
        val fileList: MutableList<DocumentFile> = mutableListOf()
        val onDeviceMapsFolder: String = PreferencesHelper.loadOnDeviceMapsFolder()
        if (onDeviceMapsFolder.isNotEmpty()) {
            val folder: DocumentFile? = DocumentFile.fromTreeUri(context, onDeviceMapsFolder.toUri())
            if (folder != null) {
                val files = folder.listFiles()
                folder.listFiles().forEach { file ->
                    if (file.name?.endsWith(".map") == true) {
                        file
                        fileList.add(file)
                    }
                }
            }
        }
        return fileList
    }


    /* Get the FileInputStreams for all .map files stored in a selected folder on device */
    fun getOnDeviceMapFileInputStreams(context: Context, documentFiles: List<DocumentFile>): Array<FileInputStream> {
        return documentFiles.map { documentFile ->
            context.contentResolver.openInputStream(documentFile.uri) as FileInputStream
        }.toTypedArray()
    }


    /* Get the name of the on-device maps folder */
    fun getOnDeviceMapsFolderName(context: Context): String {
        val onDeviceMapsFolder: String = PreferencesHelper.loadOnDeviceMapsFolder()
        if (onDeviceMapsFolder.isNotEmpty()) {
            return DocumentFile.fromTreeUri(context, onDeviceMapsFolder.toUri())?.name ?: String()
        } else {
            return String()
        }
    }


    /* Copies file to specified target */
    fun copyFile(context: Context, originalFileUri: Uri, targetFileUri: Uri, deleteOriginal: Boolean = false) {
        val inputStream = context.contentResolver.openInputStream(originalFileUri)
        val outputStream = context.contentResolver.openOutputStream(targetFileUri)
        if (outputStream != null) {
            inputStream?.copyTo(outputStream)
        }
        if (deleteOriginal) {
            context.contentResolver.delete(originalFileUri, null, null)
        }
    }


    /* Creates Uri for tracklist file */
    private fun getTracklistFileUri(context: Context): Uri {
        return File(context.getExternalFilesDir(""), Keys.TRACKLIST_FILE).toUri()
    }


    /* Converts track to JSON */
    private fun getTrackJsonString(track: Track): String {
        val gson: Gson = getCustomGson()
        var json: String = String()
        try {
            json = gson.toJson(track)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return json
    }


    /* Creates a Gson object */
    private fun getCustomGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setDateFormat("M/d/yy hh:mm a")
        gsonBuilder.excludeFieldsWithoutExposeAnnotation()
        return gsonBuilder.create()
    }


    /* Converts byte value into a human readable format */
    // Source: https://programming.guide/java/formatting-byte-size-to-human-readable-format.html
    fun getReadableByteCount(bytes: Long, si: Boolean = true): String {

        // check if Decimal prefix symbol (SI) or Binary prefix symbol (IEC) requested
        val unit: Long = if (si) 1000L else 1024L

        // just return bytes if file size is smaller than requested unit
        if (bytes < unit) return "$bytes B"

        // calculate exp
        val exp: Int = (ln(bytes.toDouble()) / ln(unit.toDouble())).toInt()

        // determine prefix symbol
        val prefix: String = ((if (si) "kMGTPE" else "KMGTPE")[exp - 1] + if (si) "" else "i")

        // calculate result and set number format
        val result: Double = bytes / unit.toDouble().pow(exp.toDouble())
        val numberFormat = NumberFormat.getNumberInstance()
        numberFormat.maximumFractionDigits = 1

        return numberFormat.format(result) + " " + prefix + "B"
    }


    /* Reads InputStream from file uri and returns it as String */
    private fun readTextFile(context: Context, fileUri: Uri): String {
        // todo read https://commonsware.com/blog/2016/03/15/how-consume-content-uri.html
        // https://developer.android.com/training/secure-file-sharing/retrieve-info
        val file: File = fileUri.toFile()
        // check if file exists
        if (!file.exists()) {
            return String()
        }
        // read until last line reached
        val stream: InputStream = file.inputStream()
        val reader: BufferedReader = BufferedReader(InputStreamReader(stream))
        val builder: StringBuilder = StringBuilder()
        reader.forEachLine {
            builder.append(it)
            builder.append("\n") }
        stream.close()
        return builder.toString()
    }


    /* Writes given text to file on storage */
    private fun writeTextFile(text: String, fileUri: Uri) {
        if (text.isNotEmpty()) {
            val file: File = fileUri.toFile()
            file.writeText(text)
        } else {
            Log.w(TAG, "Writing text file $fileUri failed. Empty text string text was provided.")
        }
    }


    /* Writes given bitmap as image file to storage */
    private fun writeImageFile(context: Context, bitmap: Bitmap, file: File, format: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG, quality: Int = 75) {
        if (file.exists()) file.delete ()
        try {
            val out = FileOutputStream(file)
            bitmap.compress(format, quality, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}