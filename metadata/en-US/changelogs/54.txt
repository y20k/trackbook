# v2.2.3 - Any Colour You Like

**2024-08-29**

- Trackbook can now use offline Mapsforge vector maps. This feature needs to be enabled in Settings. Those .map files can be downloaded for example here: https://ftp-stud.hs-esslingen.de/pub/Mirrors/download.mapsforge.org/maps/
- The Trackbook project now uses a private issue tracker.