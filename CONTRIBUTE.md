How to contribute to Trackbook
==============================

### Help with translations
The translations are managed on [Weblate](https://hosted.weblate.org/projects/trackbook/strings/).
Help is much appreciated.

### Report a bug or suggest a new feature
Trackbook does **not** have a public issue tracker for feature requests and bug reports. The app is a one-person project. Maintaining an issue tracker is too time consuming.

### Submit your own solutions
Trackbook does **not** accept pull requests. The app is a one-person project. Managing pull requests is too time consuming.